import sys
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import Qt

class MainWindow(QtGui.QMainWindow):

    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.filename = ""

        self.initUI()

    def initUI(self):

        self.textBox = QtGui.QTextEdit(self)
        
        self.initToolbar()
        self.initMenubar()

        # Set the tab stop width to around 33 pixels which is
        # about 8 spaces
        self.textBox.setTabStopWidth(33)
        self.setCentralWidget(self.textBox)

        # Initialize a statusbar for the window
        self.statusbar = self.statusBar()

        # x and y coordinates on the screen, width, height
        self.setGeometry(100,100,800,600)
        self.setWindowTitle("Editor")
        self.setWindowIcon(QtGui.QIcon("Icons/icon.png"))

    



    def initToolbar(self):

        # New action
        self.newAction = QtGui.QAction(QtGui.QIcon("Icons/new.png"),"New",self)
        self.newAction.setStatusTip("Open existing document")
        self.newAction.setShortcut("Ctrl+N")
        self.newAction.triggered.connect(self.new)


        # Open action
    	self.openAction = QtGui.QAction(QtGui.QIcon("Icons/open.png"),"Open",self)
        self.openAction.setStatusTip("Open existing document")
        self.openAction.setShortcut("Ctrl+O")
        self.openAction.triggered.connect(self.open)

        # Save action
        self.saveAction = QtGui.QAction(QtGui.QIcon("Icons/save.png"), "Save", self)
        self.saveAction.setStatusTip("Save File")
        self.saveAction.setShortcut("Ctrl+S")
        self.saveAction.triggered.connect(self.save)

        # Save As action
        self.save_asAction = QtGui.QAction(QtGui.QIcon("Icons/save_as.png"), "Save As...", self)
        self.save_asAction.setStatusTip("Save as File")
        self.save_asAction.triggered.connect(self.save_as)

        # Print action
        self.printAction = QtGui.QAction(QtGui.QIcon("Icons/print.png"), "Print", self)
        self.printAction.setStatusTip("Print document")
        self.printAction.setShortcut("Ctrl+P")
        self.printAction.triggered.connect(self.printDoc)

        # Print previewaction
        self.pr_previewAction = QtGui.QAction(QtGui.QIcon("Icons/print-preview.png"), "Previw", self)
        self.pr_previewAction.setStatusTip("Print preview")
        self.pr_previewAction.triggered.connect(self.printPreview)

        # Exit
        self.exitAction = QtGui.QAction(QtGui.QIcon('Icons/exit.png'), 'Exit', self)
        self.exitAction.setShortcut('Ctrl+Q')
        self.exitAction.setStatusTip('Exit application')
        self.exitAction.triggered.connect(self.exit)

        # Undo
        self.undoAction = QtGui.QAction(QtGui.QIcon('Icons/undo.png'), 'Undo', self)
        self.undoAction.setShortcut('Ctrl+Z')
        self.undoAction.setStatusTip('Undo')
        self.undoAction.triggered.connect(self.textBox.undo)

        # Redo
        self.redoAction = QtGui.QAction(QtGui.QIcon('Icons/redo.png'), 'Redo', self)
        self.redoAction.setStatusTip('Redo')
        self.redoAction.triggered.connect(self.textBox.redo)

        # Font bold
        self.boldAction = QtGui.QAction(QtGui.QIcon('Icons/bold.png'), 'Bold', self)
        self.boldAction.setStatusTip("Bold")
        self.boldAction.triggered.connect(self.bold)

        # Font italic
        self.italicAction = QtGui.QAction(QtGui.QIcon('Icons/italic.png'), 'Italic', self)
        self.italicAction.setStatusTip("Italic")
        self.italicAction.triggered.connect(self.italic)

        # Font underline
        self.underlineAction = QtGui.QAction(QtGui.QIcon('Icons/underline.png'), 'Underline', self)
        self.underlineAction.setStatusTip("Underline")
        self.underlineAction.triggered.connect(self.underline)



        self.toolbar = self.addToolBar("Options")

        self.toolbar.addAction(self.newAction)
        self.toolbar.addAction(self.openAction)
        self.toolbar.addAction(self.saveAction)
        self.toolbar.addAction(self.save_asAction)
        self.toolbar.addSeparator()

        self.toolbar.addAction(self.printAction)
        self.toolbar.addAction(self.pr_previewAction)
        self.toolbar.addSeparator()

        self.toolbar.addAction(self.undoAction)
        self.toolbar.addAction(self.redoAction)
        self.toolbar.addSeparator()

        self.toolbar.addAction(self.boldAction)
        self.toolbar.addAction(self.italicAction)
        self.toolbar.addAction(self.underlineAction)



    def initMenubar(self):

    	menubar = self.menuBar()
        # File menu
    	fm = menubar.addMenu("&File")

    	fm.addAction(self.newAction)
        fm.addAction(self.openAction)
        fm.addSeparator()
        fm.addAction(self.saveAction)
        fm.addAction(self.save_asAction)
        fm.addSeparator()
        fm.addAction(self.printAction)
        fm.addAction(self.pr_previewAction)
        fm.addSeparator()
    	fm.addAction(self.exitAction)

        em = menubar.addMenu("&Edit")
        em.addAction(self.undoAction)
        em.addAction(self.redoAction)
        em.addSeparator()

    # New
    def new(self):
        self.filename = ""
        self.textBox.setText("")

    # Open file
    def open(self):

        # Get filename 
        self.filename = QtGui.QFileDialog.getOpenFileName(self, 'Open File',".","(*.*)")

        if self.filename:
            fileObject = open(self.filename,"rt")
            data = fileObject.read()
            self.textBox.setText(data)

    # Application quit
    def exit(self):
        QtGui.QApplication.quit()

    # Save file
    def save(self):
        if not self.filename:
            self.filename = QtGui.QFileDialog.getSaveFileName(self, "Save File")
        try:
            fileObject = open(self.filename, "wt")
        except:
            QtGui.QMessageBox.critical(self, "Error", "Can not write in file", QtGui.QMessageBox.Close)
        else:
            fileObject.write(self.textBox.toPlainText())
            fileObject.close()

    # Save As file
    def save_as(self):
        self.filename = QtGui.QFileDialog.getSaveFileName(self, "Save File")
        try:
            fileObject = open(self.filename, "wt")
        except:
            QtGui.QMessageBox.critical(self, "Error", "Can not write in file", QtGui.QMessageBox.Close)
        else:
            fileObject.write(self.textBox.toPlainText())
            fileObject.close()

    # Print document
    def printDoc(self):
        dialog = QtGui.QPrintDialog()
        if dialog.exec_() == QtGui.QDialog.Accepted:
            self.textBox.document().print_(dialog.printer())

    # Print preview
    def printPreview(self):
        dialog = QtGui.QPrintPreviewDialog()
        dialog.paintRequested.connect(self.textBox.print_)
        dialog.exec_()

    # Font bold
    def bold(self):
        if self.textBox.fontWeight() == QtGui.QFont.Bold:
            self.textBox.setFontWeight(QtGui.QFont.Normal)
        else:
            self.textBox.setFontWeight(QtGui.QFont.Bold)

    # Font italic
    def italic(self):
        fontStatus = self.textBox.fontItalic()
        self.textBox.setFontItalic(not fontStatus)

    # Font underline
    def underline(self):
        fontStatus = self.textBox.fontUnderline()
        self.textBox.setFontUnderline(not fontStatus)
    


def main():

    app = QtGui.QApplication(sys.argv)

    main = MainWindow()
    main.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()